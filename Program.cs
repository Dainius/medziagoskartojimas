﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedziagosKartojimas
{
    class Program
    {
        static void Main(string[] args)
        {
            var aKlase = new AKlase("mama");
            Console.WriteLine($"aKlase.NameKodas(2)      { aKlase.NameKodas(2)}");
            Console.WriteLine($"aKlase.NameMetodas()     { aKlase.NameMetodas()}");
            Console.WriteLine("--------------------------");

            var bKlase = new BKlase("mama");
            Console.WriteLine($"ZodisBeBalsiu: { bKlase.ZodisBeBalsiu()}");
            Console.WriteLine($"ZodisBePriebalsiu: { bKlase.ZodisBePriebalsiu()}");
            Console.WriteLine($"ZodisSuPakeistomisBalsemis i X: { bKlase.ZodisSuPakeistomisBalsemis('X')}");
            Console.WriteLine($"ZodisSuPakeistomisPriebalsemis i 8: { bKlase.ZodisSuPakeistomisPriebalsemis(8)}");
            Console.WriteLine("--------------------------");


            var cKlase = new CKlase("mama");
            Console.WriteLine($"cKlase.NameKodas(2) Be parametrų.     { cKlase.NameKodas(2)}");


            cKlase = new CKlase("mama", 2);
            Console.WriteLine($"cKlase.NameKodas(2) Inicializuoja tik Skaičius propertį.    { cKlase.NameKodas(2)}");


            cKlase = new CKlase("mama", 2, "zodis");
            Console.WriteLine($"cKlase.NameKodas(2) Inicializuoja tik Skaičius ir Zodis properčius.    { cKlase.NameKodas(2)}");


            cKlase = new CKlase("mama", 2, "zodis", 3.14, aKlase);
            Console.WriteLine($"cKlase.NameKodas(2) Inicializuoja visus properčius.     { cKlase.NameKodas(2)}");

            Console.ReadKey();
        }
    }
}
