﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedziagosKartojimas
{
    interface IZodisKitaip
    {
        string ZodisBeBalsiu();
        string ZodisBePriebalsiu();
        string ZodisSuPakeistomisBalsemis(char symbol);
        string ZodisSuPakeistomisPriebalsemis(int number);

    }
}
