﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedziagosKartojimas
{
    public class AKlase : BaseClass
    {
        public AKlase(string name) : base(name)
        {

        }
        public override int NameKodas(int number)
        {
            int result = 0;

            foreach (var item in Name.ToLower().ToArray())
            {
                result += number * (int)item;
            }
            return result;
        }

        public override string NameMetodas()
        {
            string name = string.Empty;

            string syl = "EYUIOA".ToLower();

            foreach (var item in Name.ToLower().ToArray())
            {
                if (syl.Contains(item))
                {
                    name += $"{(int)item}";
                }
                else
                {
                    name += item;
                }
            }
            return name;
        }
    }
}
