﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedziagosKartojimas
{
    class CKlase : BKlase
    {
        public int Skaicius { get; set; }
        public string Zodis { get; set; }
        public double DoubleSkaicius { get; set; }
        public AKlase AObjektas { get; set; }
        public CKlase(string name) : base(name)
        {
        }

        public CKlase(string name, int skaicius ) : base(name)
        {
            this.Skaicius = skaicius;
        }

        public CKlase(string name, int skaicius, string zodis) : base(name)
        {
            this.Skaicius = skaicius;
            this.Zodis = zodis;
        }

        public CKlase(string name, int skaicius, string zodis, double doubleSkaicius, AKlase aKlase) : base(name)
        {
            this.Skaicius = skaicius;
            this.Zodis = zodis;
            this.DoubleSkaicius = doubleSkaicius;
            this.AObjektas = aKlase;
        }
    }
}
