﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedziagosKartojimas
{
    public class BKlase : BaseClass, IZodisKitaip
    {
        public BKlase(string name) : base(name)
        {
            
        }

        public override int NameKodas(int number)
        {
            int result = 0;

            foreach (var item in Name.ToLower().ToArray())
            {
                result += number - (int)item;
            }
            return result;
        }

        public override string NameMetodas()
        {
            throw new NotImplementedException();
        }

        public string ZodisBeBalsiu()
        {
            string name = string.Empty;
            string cons = "QWRTPSDFGHJKLZXCVBNM".ToLower();

            foreach (var item in Name.ToLower().ToArray())
            {
                if (cons.Contains(item))
                {
                    name += item;
                }
            }
            return name;
        }

        public string ZodisBePriebalsiu()
        {
            string name = string.Empty;
            string cons = "QWRTPSDFGHJKLZXCVBNM".ToLower();

            foreach (var item in Name.ToLower().ToArray())
            {
                if (!cons.Contains(item))
                {
                    name += item;
                }
            }
            return name;
        }

        public string ZodisSuPakeistomisBalsemis(char symbol)
        {
            string name = string.Empty;
            string cons = "QWRTPSDFGHJKLZXCVBNM".ToLower();

            foreach (var item in Name.ToLower().ToArray())
            {
                if (!cons.Contains(item))
                {
                    name += symbol;
                }
                else
                {
                    name += item;
                }
            }
            return name;
        }

        public string ZodisSuPakeistomisPriebalsemis(int number)
        {
            string name = string.Empty;
            string cons = "QWRTPSDFGHJKLZXCVBNM".ToLower();

            foreach (var item in Name.ToLower().ToArray())
            {
                if (!cons.Contains(item))
                {
                    name += item;
                }
                else
                {
                    name += $"{number}";
                }
            }
            return name;
        }
    }
}
